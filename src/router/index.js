import Vue from 'vue';
import Router from 'vue-router';
import Meta from 'vue-meta';
import Home from '@/components/Home';
import Login from '@/components/Login';
import Chat from '@/components/Chat';
import AuthGuard from './auth-guard';

Vue.use(Router);
Vue.use(Meta);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      beforeEnter: AuthGuard,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/chat/:msgId/:name',
      name: 'Chat',
      component: Chat,
      beforeEnter: AuthGuard,
    },
  ],
});
