// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuetify from 'vuetify';
import * as firebase from 'firebase';
import App from './App';
import router from './router';
import store from './store';
import './stylus/main.styl';

Vue.use(Vuetify);
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  beforeCreate() {
    const config = {
      apiKey: 'AIzaSyBHl1KvuV2_PD6KaE2DWLznBhxY_bCNadI',
      authDomain: 'chataoo-5021c.firebaseapp.com',
      databaseURL: 'https://chataoo-5021c.firebaseio.com',
      projectId: 'chataoo-5021c',
      storageBucket: 'chataoo-5021c.appspot.com',
      messagingSenderId: '905061297120',
    };
    firebase.initializeApp(config);
    this.$store.dispatch('loader', true);
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoLogin', user);
        const amOnline = firebase.database().ref('/.info/connected');
        const db = firebase.database().ref(`/user/${user.uid}/online`);
        amOnline.on('value', (snapshot) => {
          if (snapshot.val()) {
            db.onDisconnect().set(false);
            db.set(true);
          }
        });
      }
      this.$store.dispatch('loader', false);
    });
  },
});
