export default {
  user(state) {
    return state.user;
  },
  isLoading(state) {
    return state.isLoading;
  },
  chats(state) {
    return state.chats;
  },
  isMsgLoading(state) {
    return state.isMsgLoading;
  },
  messages(state) {
    return state.messages;
  },
};
