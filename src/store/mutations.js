import Vue from 'vue';

export default {
  SET_LOADER(state, torf) {
    Vue.set(state, 'isLoading', torf);
  },
  SHOW_MSG_LOADER(state, torf) {
    Vue.set(state, 'isMsgLoading', torf);
  },
  SET_USER(state, user) {
    Vue.set(state, 'user', user);
  },
  PUSH_CHAT_TO_STORE(state, chat) {
    // eslint-disable-next-line
    var flag = true;
    if (chat) {
      state.chats.forEach((chatPre, i) => {
        if (chatPre.id === chat.id) {
          Vue.set(state.chats, i, chat);
          flag = false;
        }
      });
      if (flag) {
        state.chats.push(chat);
      }
    } else {
      Vue.set(state, 'chats', []);
    }
  },
  PUSH_MSG_TO_STORE(state, msgs) {
    Vue.set(state, 'messages', msgs);
    Vue.set(state, 'isMsgLoading', false);
  },
};
