import authActions from './actions/auth';
import chatActions from './actions/chat';

export default {
  ...authActions,
  ...chatActions,
  loader({ commit }, payload) {
    commit('SET_LOADER', payload);
  },
};
