import Vue from 'vue';
import Vuex from 'vuex';
import mutations from './mutations';
import actions from './actions';
import getters from './getters';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    user: null,
    isLoading: false,
    isMsgLoading: false,
    chats: [],
    messeges: [],
  },
  actions,
  mutations,
  getters,
});

export default store;
