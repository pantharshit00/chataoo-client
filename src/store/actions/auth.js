import * as firebase from 'firebase';

export default {
  login({ commit }, payload) {
    commit('SET_LOADER', true);
    switch (payload.provider) {
      case 'facebook': {
        firebase.auth().signInWithPopup(new firebase.auth.FacebookAuthProvider())
          .then(({ user }) => {
            commit('SET_LOADER', false);
            const newUser = {
              name: user.displayName,
              email: user.email,
              photoURL: user.photoURL,
              id: user.uid,
            };
            commit('SET_USER', newUser);
          })
          .catch(() => {
            commit('SET_LOADER', false);
          });
        break;
      }
      case 'google': {
        firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider())
          .then(({ user }) => {
            commit('SET_LOADER', false);
            const newUser = {
              name: user.displayName,
              email: user.email,
              photoURL: user.photoURL,
              id: user.uid,
            };
            commit('SET_USER', newUser);
          })
          .catch(() => {
            commit('SET_LOADER', false);
          });
        break;
      }
      case 'twitter': {
        firebase.auth().signInWithPopup(new firebase.auth.TwitterAuthProvider())
          .then(({ user }) => {
            commit('SET_LOADER', false);
            const newUser = {
              name: user.displayName,
              email: user.email,
              photoURL: user.photoURL,
              id: user.uid,
            };
            commit('SET_USER', newUser);
          })
          .catch(() => {
            commit('SET_LOADER', false);
          });
        break;
      }
      default: commit('SET_LOADER', false);
    }
  },
  autoLogin({ commit }, user) {
    const newUser = {
      name: user.displayName,
      email: user.email,
      photoURL: user.photoURL,
      id: user.uid,
    };
    commit('SET_USER', newUser);
  },
};
