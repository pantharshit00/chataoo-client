import { database as db } from 'firebase';

export default {
  getChats(context) {
    context.commit('SHOW_MSG_LOADER', true);
    const uid = context.getters.user.id;
    db().ref(`/user/${uid}`).on('value', (snap) => {
      const value = snap.val();
      const chats = value.chats ? Object.values(value.chats) : [];
      chats.forEach((chat) => {
        db().ref(`/chats/${chat}`).on('value', (snap2) => {
          const snapshot = snap2.val();
          if (snapshot) {
            const receiver = snapshot.s_1 !== uid ? snapshot.s_1 : snapshot.s_2;
            db().ref(`/user/${receiver}`).on('value', (snap3) => {
              const recDetails = snap3.val();
              db().ref(`/msgColl/${snapshot.msgCollRef}`).limitToLast(1).on('value', (snap4) => {
                const ss = snap4.val();
                const lastMsg = ss ? Object.values(ss)[0] : '';
                context.commit('PUSH_CHAT_TO_STORE', {
                  sender: recDetails.name,
                  id: receiver,
                  online: recDetails.online,
                  msgCollRef: snapshot.msgCollRef,
                  lastMsg: lastMsg.data,
                  lastMsgStatus: 'Sent',
                  imageURL: recDetails.photo,
                });
              });
            });
          }
        });
      });
      context.commit('SHOW_MSG_LOADER', false);
    });
  },
  getMesseges(context, { msgColl }) {
    context.commit('SHOW_MSG_LOADER', true);
    db().ref(`/msgColl/${msgColl}`).on('value', (snap) => {
      const val = snap.val();
      if (val) {
        context.commit('PUSH_MSG_TO_STORE', val);
      } else {
        context.commit('SHOW_MSG_LOADER', false);
      }
    });
  },
};
